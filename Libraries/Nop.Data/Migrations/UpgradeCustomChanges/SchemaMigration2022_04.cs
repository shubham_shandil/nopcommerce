﻿using FluentMigrator;
using FluentMigrator.Builders.Delete;
using FluentMigrator.Builders.Rename;
using Nop.Data.Mapping;
using System.Collections.Generic;
using Nop.Core.Domain.Localization;
using System.Linq;
using Nop.Core.Domain.Tasks;
using Nop.Core.Domain.Configuration;
using Nop.Core.Domain.Directory;
using System;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Nop.Core.Domain.Messages;


namespace Nop.Data.Migrations.UpgradeCustomChanges
{
    [NopMigration("2022/04/04 02:30:08:9087352", "SchemaMigration2022_04")]
    public class SchemaMigration2022_04 : AutoReversingMigration
    {
        #region Fields

        private readonly IMigrationManager _migrationManager;
        private readonly INopDataProvider _dataProvider;

        #endregion

        #region Ctor

        public SchemaMigration2022_04(IMigrationManager migrationManager,
            INopDataProvider dataProvider)
        {
            _migrationManager = migrationManager;
            _dataProvider = dataProvider;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Collect the UP migration expressions
        /// </summary>
        public async override void Up()
        {
            #region Resources

            var resources = new Dictionary<string, string>();
            var languages = _dataProvider.QueryAsync<Language>($"Select * from {nameof(Language)}").Result;

            var localeStringResource = _dataProvider.QueryAsync<int>
               ($"Select count(id) from {nameof(LocaleStringResource)} Where {nameof(LocaleStringResource.ResourceName)} = 'ShoppingCart.IsAutoShipping'").Result;
            if (localeStringResource.FirstOrDefault() == 0)
            {
                resources.Add("ShoppingCart.IsAutoShipping", "Auto-Ship Click Yes No");
                resources.Add("ShoppingCart.YouSaved", "You Saved");
            }





            //insert new locale resources
            var locales = languages.SelectMany(language => resources.Select(resource => new LocaleStringResource
            {
                LanguageId = language.Id,
                ResourceName = resource.Key,
                ResourceValue = resource.Value
            })).ToList();

            foreach (var res in locales)
                _dataProvider.InsertEntity(res);

            List<string> updateResources = new List<string>(new string[] {
                //"Update LocaleStringResource set ResourceValue='You Saved' where ResourceName='ShoppingCart.ItemYouSave'",
            });

            foreach (var res in updateResources)
            {
                _dataProvider.ExecuteNonQueryAsync(res);
            }

            #endregion
        }

        #endregion
    }
}