﻿using FluentMigrator;
using FluentMigrator.Builders.Delete;
using FluentMigrator.Builders.Rename;
using Nop.Data.Mapping;
using System.Collections.Generic;
using Nop.Core.Domain.Localization;
using System.Linq;
using Nop.Core.Domain.Tasks;
using Nop.Core.Domain.Configuration;
using Nop.Core.Domain.Directory;
using System;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Nop.Data.Extensions;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Orders;

namespace Nop.Data.Migrations.UpgradeCustomChanges
{
    [NopMigration("2022/04/10 20:14:08:9037352", "SchemaMigration2022_04_Columns")]

    public class SchemaMigration2022_04_Columns : AutoReversingMigration
    {
        #region Fields

        private readonly IMigrationManager _migrationManager;
        private readonly INopDataProvider _dataProvider;

        #endregion

        #region Ctor

        public SchemaMigration2022_04_Columns(IMigrationManager migrationManager,
            INopDataProvider dataProvider)
        {
            _migrationManager = migrationManager;
            _dataProvider = dataProvider;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Collect the UP migration expressions
        /// </summary>
        public override void Up()
        {
            if (!Schema.Table(NameCompatibilityManager.GetTableName(typeof(ShoppingCartItem))).Column(nameof(ShoppingCartItem.IsAutoShipping)).Exists())
            {
                //add new column
                Alter.Table(NameCompatibilityManager.GetTableName(typeof(ShoppingCartItem)))
                    .AddColumn(nameof(ShoppingCartItem.IsAutoShipping)).AsBoolean().NotNullable().WithDefaultValue(0);
            }
            if (!Schema.Table(NameCompatibilityManager.GetTableName(typeof(OrderItem))).Column(nameof(OrderItem.IsAutoShipping)).Exists())
            {
                //add new column
                Alter.Table(NameCompatibilityManager.GetTableName(typeof(OrderItem)))
                    .AddColumn(nameof(OrderItem.IsAutoShipping)).AsBoolean().NotNullable().WithDefaultValue(0);
            }
        }

        #endregion
    }
}
