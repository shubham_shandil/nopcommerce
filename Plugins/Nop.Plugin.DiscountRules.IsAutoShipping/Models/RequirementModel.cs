﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Plugin.DiscountRules.IsAutoShipping.Models
{
    public class RequirementModel
    {
        public RequirementModel()
        {
        }

        [NopResourceDisplayName("Plugins.DiscountRules.IsAutoShipping.Fields.VerifyCartOnly")]
        public bool VerifyCartOnly { get; set; }

        public int DiscountId { get; set; }

        public int RequirementId { get; set; }
    }
}