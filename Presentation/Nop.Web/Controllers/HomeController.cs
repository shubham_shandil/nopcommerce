﻿using System;
using System.Net.Http;
using System.Net.Http.Json;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Nop.Web.Models.Checkout;

namespace Nop.Web.Controllers
{
    public partial class HomeController : BasePublicController
    {
        public virtual IActionResult Index()
        {
            // test commit
           // ServiceCall();
            return View();
        }

        public Int32 ServiceCall()
        {
            string ApiBaseUrl = "http://localhost:62170/api/";
            //string ApiBaseUrl = "http://182.76.115.146:88/api/";
            CommissionResponseViewModel respponse = new CommissionResponseViewModel();
            DoCommissionModel CommissionModel = new DoCommissionModel();
            CommissionModel.UserId = 21;
            CommissionModel.OrderId = 1;

            using (var client = new HttpClient())
            {
                client.Timeout = TimeSpan.FromMinutes(3);
                client.BaseAddress = new Uri(ApiBaseUrl + "Commission/");
                HttpResponseMessage responseData = client.PostAsJsonAsync("DoCommission", CommissionModel).Result;

                if (responseData.IsSuccessStatusCode)
                {
                    try
                    {
                        var JsonString = responseData.Content.ReadAsStringAsync();
                        respponse = JsonConvert.DeserializeObject<CommissionResponseViewModel>(JsonString.Result);

                        if (string.Equals(respponse.Message.ToLower(), "Success"))
                        {
                            return 1;
                        }
                        else
                        {
                            return 0;
                        }
                    }
                    catch (Exception ex)
                    {
                        _ = ex.Message;
                    }
                }
            }

            return respponse.Id;
        }
        
    }
}