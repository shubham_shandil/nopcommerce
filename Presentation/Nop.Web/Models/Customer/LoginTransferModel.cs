﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Models.Customer
{
    public class LoginTransferModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
    public class OrderViewModel
    {
        public decimal? OrderTotal { get; set; }
        public List<OrderItems> Items { get; set; }
    }
    public class OrderItems
    {
        public int ItemId { get; set; }
        public decimal Price { get; set; }
        public decimal Qty { get; set; }
        public decimal Total { get; set; }
        public bool AutoShip { get; set; }
    }
}
