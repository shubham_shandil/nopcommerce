﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Models.Checkout
{
    public class CommissionResponseViewModel
    {
        public Int32 Id { get; set; }
        public string Message { get; set; }
    }
}
